import 'package:fastotv_device_info/device.dart';
import 'package:fastotv_device_info/src/android.dart' as android_device;
import 'package:fastotv_device_info/src/ios.dart' as ios_device;

const LGE_BRAND = 'LGE';
const APPLE_BRAND = 'Apple';
const AMAZON_BRAND = 'Amazon';
const TCL_BRAND = 'TCL';
const VS_BRAND = 'VS';
const MTK_BRAND = 'MTK';
const ROCKCHIP_BRAND = 'rockchip';
const SONY_BRAND = 'Sony';
const NVIDIA_BRAND = 'NVIDIA';
const SAMSUNG_BRAND = 'samsung';
const GOOGLE_BRAND = 'Google';
const AMLOGIC_BRAND = 'Amlogic';
const ALLWINNER_BRAND = 'Allwinner';
const INVIN_BRAND = 'dolphin';
const FORMULER_BRAND = 'FORMULER';
const XIAOMI_BRAND = 'Xiaomi';

abstract class Devices {
  static const Map<String, List<Device>> all = {
    APPLE_BRAND: ios,
    LGE_BRAND: lge,
    AMAZON_BRAND: amazon,
    TCL_BRAND: tcl,
    VS_BRAND: vs,
    MTK_BRAND: mtk,
    ROCKCHIP_BRAND: rockchip,
    SONY_BRAND: sony,
    NVIDIA_BRAND: nvidia,
    SAMSUNG_BRAND: samsung,
    AMLOGIC_BRAND: amlogic,
    ALLWINNER_BRAND: allwinner,
    INVIN_BRAND: invin,
    FORMULER_BRAND: formuler,
    GOOGLE_BRAND: google,
    XIAOMI_BRAND: xiaomi
  };

  static const ios = <Device>[
    ios_device.iPhone5s,
    ios_device.iPhone8,
    ios_device.iPhoneXr,
    ios_device.iPhoneXs,
    ios_device.iPadAir2,
    ios_device.iPadAir_3,
    ios_device.iPadPro_129_2
  ];

  static const lge = <Device>[android_device.lgeNexus5];
  static const amazon = <Device>[
    android_device.AmazonFireTVStickLite,
    android_device.AmazonFireTVStick,
    android_device.AmazonFireTVStick4K
  ];
  static const tcl = <Device>[android_device.SmartTVPro];
  static const vs = <Device>[android_device.vsM8sPlusW];
  static const mtk = <Device>[android_device.mtkAsano32LH7010T];
  static const rockchip = <Device>[
    android_device.rockchipH96Max2,
    android_device.rockchipTX2,
    android_device.rockchipBil,
    android_device.rockchipH96Max,
    android_device.rockchipX10PRO,
    android_device.rockchipA95XR3
  ];
  static const sony = <Device>[android_device.sonyBravia2015];
  static const nvidia = <Device>[android_device.nvidiaShield];
  static const samsung = <Device>[
    android_device.samsungNote8,
    android_device.samsungGalaxy6SPlus,
    android_device.samsungSMN975F,
    android_device.samsungSMT500,
    android_device.samsungSMG532F
  ];
  static const amlogic = <Device>[
    android_device.amlogicH96PROPlus,
    android_device.amlogicT95ZPlus,
    android_device.amlogicH96PRO,
    android_device.amlogicP281,
    android_device.amlogicP2128189,
    android_device.amlogicTX3,
    android_device.amlogicTX5Max,
    android_device.amlogicTX6,
    android_device.amlogicTX9Pro,
    android_device.amlogicX92,
    android_device.amlogicX923GB,
    android_device.amlogicMXQPROPlus,
    android_device.amlogicPendooX11PRO,
    android_device.amlogicCabMxq4kG2
  ];
  static const invin = <Device>[android_device.invinIPC002];
  static const allwinner = <Device>[android_device.allwinnerMBOX];
  static const formuler = <Device>[
    android_device.formulerZx,
    android_device.formulerZ7Plus,
    android_device.formulerZ7Plus5G
  ];
  static const google = <Device>[android_device.googlePixel4a, android_device.googleMBOX];
  static const xiaomi = <Device>[android_device.xiaomiMIBOX4];
}
