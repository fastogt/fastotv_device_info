import 'package:fastotv_device_info/device.dart';
import 'package:flutter/widgets.dart';

// LGE Nexus 5
const lgeNexus5 = AndroidDevice(
    name: "Nexus 5",
    model: "Nexus 5",
    landscape: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(592, 360), devicePixelRatio: 3.0),
    portrait: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(360, 592), devicePixelRatio: 3.0));

const mtkAsano32LH7010T = AndroidDeviceWithoutTouch(
    name: "Asano 32LH7010T",
    model: "32LH7010T",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const SmartTVPro = AndroidDeviceWithoutTouch(
    name: "Smart TV Pro",
    model: "Smart TV Pro",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const vsM8sPlusW = AndroidDevice(
    name: "Mecool M8S PLUS W",
    model: "M8S PLUS W",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const AmazonFireTVStickLite = AndroidDevice(
    name: "Fire TV Stick Lite",
    model: "AFTSS",
    landscape: MediaQueryData(size: Size(960, 540), devicePixelRatio: 2.0),
    portrait: MediaQueryData(size: Size(540, 960), devicePixelRatio: 2.0));

const AmazonFireTVStick = AndroidDevice(
    name: "Fire TV Stick",
    model: "AFTT",
    landscape: MediaQueryData(size: Size(960, 540), devicePixelRatio: 2.0),
    portrait: MediaQueryData(size: Size(540, 960), devicePixelRatio: 2.0));

const AmazonFireTVStick4K = AndroidDevice(
    name: "Fire TV Stick 4K",
    model: "AFTMM",
    landscape: MediaQueryData(size: Size(960, 540), devicePixelRatio: 2.0),
    portrait: MediaQueryData(size: Size(540, 960), devicePixelRatio: 2.0));

const rockchipBil = AndroidDeviceWithoutTouch(
    name: "Bil3arabiTV",
    model: "BIL3ARABITV-RK3318",
    landscape: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(720, 1280), devicePixelRatio: 1.5));

const rockchipTX2 = AndroidDeviceWithoutTouch(
    name: "Tanix TX2",
    model: "TX2",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const rockchipH96Max = AndroidDevice(
    name: "H96 Max",
    model: "H96 Max zrK3318",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const rockchipH96Max2 = AndroidDeviceWithoutTouch(
    name: "H96 Max 2",
    model: "H96 Max",
    landscape: MediaQueryData(size: Size(1280, 678), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(678, 1280), devicePixelRatio: 1.5));

const rockchipX10PRO = AndroidDeviceWithoutTouch(
    name: "X10 PRO",
    model: "X10PRO",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const rockchipA95XR3 = AndroidDeviceWithoutTouch(
    name: "A95 XR3",
    model: "A95XR3",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const amlogicH96PROPlus = AndroidDeviceWithoutTouch(
    name: "H96 PRO+",
    model: "H96 PRO+",
    landscape: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(1280, 672), devicePixelRatio: 1.5),
    portrait: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(672, 1280), devicePixelRatio: 1.5));

const amlogicT95ZPlus = AndroidDeviceWithoutTouch(
    name: "T95Z Plus",
    model: "T95ZPLUS",
    landscape: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(1280, 672), devicePixelRatio: 1.5),
    portrait: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(672, 1280), devicePixelRatio: 1.5));

const amlogicH96PRO = AndroidDeviceWithoutTouch(
    name: "H96 PRO",
    model: "H96 PRO",
    landscape: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(1280, 672), devicePixelRatio: 1.5),
    portrait: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(672, 1280), devicePixelRatio: 1.5));

const amlogicP281 = AndroidDeviceWithoutTouch(
    name: "P281",
    model: "p281",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const amlogicP2128189 = AndroidDeviceWithoutTouch(
    name: "p212_8189",
    model: "p212_8189",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const amlogicTX3 = AndroidDeviceWithoutTouch(
    name: "Tanix TX3",
    model: "TX3",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const amlogicTX5Max = AndroidDeviceWithoutTouch(
    name: "Amlogic TX5 Max",
    model: "TX5 MAX",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const amlogicTX6 = AndroidDeviceWithoutTouch(
    name: "Amlogic TX6",
    model: "TX6",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const amlogicTX9Pro = AndroidDeviceWithoutTouch(
    name: "Amlogic TX9 Pro",
    model: "TX9 Pro",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const amlogicX92 = AndroidDeviceWithoutTouch(
    name: "Amlogic X92",
    model: "X92",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const amlogicX923GB = AndroidDeviceWithoutTouch(
    name: "Amlogic X92 3GB",
    model: "X92_3GB",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const amlogicMXQPROPlus = AndroidDeviceWithoutTouch(
    name: "MXQ PRO+",
    model: "MXQ PRO+",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const amlogicPendooX11PRO = AndroidDeviceWithoutTouch(
    name: "Pendoo X11 PRO",
    model: "pendoo X11 PRO",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const amlogicCabMxq4kG2 = AndroidDeviceWithoutTouch(
    name: "Cab-MXQ4K-G2",
    model: "Cab-MXQ4K-G2",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const sonyBravia2015 = AndroidDevice(
    name: "Bravia 2015",
    model: "BRAVIA 2015",
    landscape: MediaQueryData(size: Size(960, 540), devicePixelRatio: 2.0),
    portrait: MediaQueryData(size: Size(540, 960), devicePixelRatio: 2.0));

const nvidiaShield = AndroidDeviceWithoutTouch(
    name: "SHIELD Android TV",
    model: "SHIELD Android TV",
    landscape: MediaQueryData(size: Size(960, 540), devicePixelRatio: 2.0),
    portrait: MediaQueryData(size: Size(540, 960), devicePixelRatio: 2.0));

const samsungNote8 = AndroidDevice(
    name: "Note 8",
    model: "SM-N950F",
    landscape: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(414.4, 797.7), devicePixelRatio: 2.625),
    portrait: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(797.7, 414.4), devicePixelRatio: 2.625));

const samsungGalaxy6SPlus = AndroidDevice(
    name: "Samsung Galaxy S6 edge+",
    model: "SM-G928V",
    landscape: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(414.4, 731.4), devicePixelRatio: 2.625),
    portrait: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(731.4, 411.4), devicePixelRatio: 2.625));

const samsungSMN975F = AndroidDeviceWithoutTouch(
    name: "Samsung TV SM-N975F",
    model: "SM-N975F",
    landscape: MediaQueryData(size: Size(1066.7, 480)),
    portrait: MediaQueryData(size: Size(480, 1066.7)));

const samsungSMT500 = AndroidDevice(
    name: "SM-T500",
    model: "SM-T500",
    landscape: MediaQueryData(size: Size(1285.3, 800), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(800, 1285.3), devicePixelRatio: 1.5));

const invinIPC002 = AndroidDeviceWithoutTouch(
    name: "Invin IPC002",
    model: "IPC002",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const allwinnerMBOX = AndroidDeviceWithoutTouch(
    name: "MBOX",
    model: "MBOX",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const formulerZx = AndroidDeviceWithoutTouch(
    name: "Zx",
    model: "Zx",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const formulerZ7Plus = AndroidDeviceWithoutTouch(
    name: "Z7 Plus",
    model: "Z7_PLUS",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const formulerZ7Plus5G = AndroidDeviceWithoutTouch(
    name: "Z7 Plus 5G",
    model: "Z7_PLUS_5G",
    landscape: MediaQueryData(size: Size(1280, 720), devicePixelRatio: 1.5),
    portrait: MediaQueryData(size: Size(720, 1280), devicePixelRatio: 1.5));

const googlePixel4a = AndroidDevice(
    name: "Pixel 4a",
    model: "Pixel 4a",
    landscape: MediaQueryData(
        padding: EdgeInsets.only(top: 49.5), size: Size(834.9, 392.7), devicePixelRatio: 2.75),
    portrait: MediaQueryData(
        padding: EdgeInsets.only(top: 49.5), size: Size(392.7, 834.9), devicePixelRatio: 2.75));

const googleMBOX = AndroidDeviceWithoutTouch(
    name: "MBOX",
    model: "MBOX",
    landscape: MediaQueryData(size: Size(1280, 720)),
    portrait: MediaQueryData(size: Size(720, 1280)));

const xiaomiMIBOX4 = AndroidDeviceWithoutTouch(
    name: "MIBOX4",
    model: "MIBOX4",
    landscape: MediaQueryData(size: Size(960, 540)),
    portrait: MediaQueryData(size: Size(540, 960)));

const samsungSMG532F = AndroidDevice(
    name: "SM-G532F",
    model: "SM-G532F",
    landscape: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(360.0, 640.0), devicePixelRatio: 1.5),
    portrait: MediaQueryData(
        padding: EdgeInsets.only(top: 24.0), size: Size(640.0, 360.0), devicePixelRatio: 1.5));
